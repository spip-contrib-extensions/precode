<?php

if (!defined("_ECRIRE_INC_VERSION")) {
	return;
}

function precode_insert_head($flux) {
	$flux .= '<link rel="stylesheet" href="' . find_in_path('css/precode.css') . '" type="text/css" media="all" />' . "\n";
	$flux .= '<script type="text/javascript" src="' . find_in_path('js/clipboard.min.js') . '"></script>' . "\n";

	// produire le js depuis un squelette pour pouvoir traduire les libellés
	$js   = produire_fond_statique('js/precode.js');
	$flux .= '<script type="text/javascript" src="' . $js . '"></script>' . "\n";

	return $flux;
}

function precode_header_prive($flux) {
	$flux .= '<link rel="stylesheet" href="' . find_in_path('css/precode.css') . '" type="text/css" media="all" />' . "\n";

	return $flux;
}
